﻿using System;
using System.Web;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using WCFSOAPService.DAL;

namespace WCFSOAPService
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            ConfigureUnity();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        private void ConfigureUnity()
        {
            // Configure common service locator to use Unity
            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(DiWrapper.Container));

            // Register the repository
            DiWrapper.Container.RegisterType<EmployeeDbContext>();
        }
    }
}
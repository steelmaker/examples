﻿using System.Collections.Generic;
using System.ServiceModel;
using WCFSOAPService.Model;
using WCFSOAPService.Services;

namespace WCFSOAPService
{
    [ServiceContract]
    public interface IEmployeeService
    {
        [OperationContract]
        [FaultContract(typeof(FaultDetail))]
        IList<Employee> GetAllEmployees();

        [OperationContract]
        [FaultContract(typeof(FaultDetail))]
        Employee Get(int id);

        [OperationContract]
        [FaultContract(typeof(FaultDetail))]
        void Add(Employee employee);

        [OperationContract]
        [FaultContract(typeof(FaultDetail))]
        void Edit(Employee employee);

        [OperationContract]
        [FaultContract(typeof(FaultDetail))]
        void Remove(int id);

        [OperationContract]
        [FaultContract(typeof(FaultDetail))]
        void Save(IList<Employee> employees);
    }
}

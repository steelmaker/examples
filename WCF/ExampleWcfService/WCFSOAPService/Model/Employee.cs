﻿namespace WCFSOAPService.Model
{
    /// <summary>
    /// Employee.
    /// </summary>
    public class Employee : Entity
    {
        /// <summary>
        /// Gets or sets employee name.
        /// </summary>
        public string Name { get; set; }
    }
}
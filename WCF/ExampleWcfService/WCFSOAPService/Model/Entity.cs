﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WCFSOAPService.Model
{
    public class Entity : IEntity
    {
        /// <summary>
        /// Database table identity
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Global identity, unique for each entity
        /// </summary>
        [Index(IsUnique = true)]
        public virtual Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        [Timestamp]
        public virtual byte[] RowVersion { get; set; }
    }
}
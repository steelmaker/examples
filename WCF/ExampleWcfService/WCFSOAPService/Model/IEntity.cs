﻿using System;
using System.ComponentModel;

namespace WCFSOAPService.Model
{
    public interface IEntity
    {
        /// <summary>
        /// Database table identity
        /// </summary>
        [Browsable(false)]
        int Id { get; set; }

        /// <summary>
        /// Global identity, unique for each entity
        /// </summary>
        [Browsable(false)]
        Guid Guid { get; set; }
    }
}
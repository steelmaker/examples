﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel.Activation;
using WCFSOAPService.DAL;
using WCFSOAPService.Model;

namespace WCFSOAPService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EmployeeService : IEmployeeService
    {
        private readonly EmployeeDbContext _db;

        public EmployeeService(EmployeeDbContext db)
        {
            _db = db;
        }

        public IList<Employee> GetAllEmployees()
        {
            return _db.Employees.ToList();
        }

        public Employee Get(int id)
        {
            return _db.Employees.Find(id);
        }

        public void Add(Employee employee)
        {
            AddEmployee(employee);

            _db.SaveChanges();
        }

        private void AddEmployee(Employee employee)
        {
            _db.Employees.Add(employee);
        }

        public void Edit(Employee employee)
        {
            if (EditEmployee(employee)) return;

            _db.SaveChanges();
        }

        private bool EditEmployee(Employee employee)
        {
            var entity = _db.Employees.Find(employee.Id);
            if (entity == null)
                return true;

            _db.Entry(entity).State = EntityState.Modified;
            entity.Name = employee.Name;
            return false;
        }

        public void Remove(int id)
        {
            var employee = _db.Employees.Find(id);
            if (employee != null)
            {
                _db.Employees.Remove(employee);
                _db.SaveChanges();
            }
        }

        public void Save(IList<Employee> employees)
        {
            foreach (var employee in employees)
            {
                if (employee.Id == 0)
                    AddEmployee(employee);
                else
                    EditEmployee(employee);
            }

            _db.SaveChanges();
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace WCFSOAPService.Services
{
    [DataContract]
    [Serializable]
    public class FaultDetail
    {
        /// <summary>
        /// Gets or sets fault detail result.
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Gets or sets error message.
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets error details.
        /// </summary>
        [DataMember]
        public string ErrorDetails { get; set; }
    }
}
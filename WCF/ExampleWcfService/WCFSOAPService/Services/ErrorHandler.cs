﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace WCFSOAPService.Services
{
    /// <summary>
    /// Handles errors in WCF services
    /// </summary>
    public class ErrorHandler : IErrorHandler, IServiceBehavior
    {
        /// <summary>
        /// Provide fault.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="version"></param>
        /// <param name="fault"></param>
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            var faultException = new FaultException("An error occurred on the server. Contact your administrator.");
            var messageFault = faultException.CreateMessageFault();
            fault = Message.CreateMessage(version, messageFault, faultException.Action);
        }

        /// <summary>
        /// HandleError. Log an error, then allow the error to be handled as usual.
        /// Return true if the error is considered as already handled
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public bool HandleError(Exception error)
        {
            //Logger logger = LogManager.GetCurrentClassLogger();
            //logger.Error("Service error", error.Message);

            // Returning true indicates you performed your behavior.
            return true;
        }

        /// <summary>
        /// Validate.
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        /// <summary>
        /// Add binding parameters.
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        /// <param name="endpoints"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
                                         BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Apply dispatch behavior.
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler errorHandler = new ErrorHandler();

            foreach (var channelDispatcherBase in serviceHostBase.ChannelDispatchers)
            {
                var channelDispatcher = channelDispatcherBase as ChannelDispatcher;
                if (channelDispatcher != null)
                    channelDispatcher.ErrorHandlers.Add(errorHandler);
            }
        }
    }

    /// <summary>
    /// Behavior error handler element.
    /// </summary>
    public class ErrorHandlerElement : BehaviorExtensionElement
    {
        /// <summary>
        /// Create behavior.
        /// </summary>
        /// <returns></returns>
        protected override object CreateBehavior()
        {
            return new ErrorHandler();
        }

        /// <summary>
        /// Gets the behavior type.
        /// </summary>
        public override Type BehaviorType
        {
            get { return typeof (ErrorHandler); }
        }
    }
}
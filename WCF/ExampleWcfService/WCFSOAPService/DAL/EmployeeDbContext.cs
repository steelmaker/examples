﻿using System;
using System.Data.Entity;
using System.Linq;
using WCFSOAPService.Model;

namespace WCFSOAPService.DAL
{
    public class EmployeeDbContext : DbContext
    {
        public EmployeeDbContext()
            : base("EmployeeDbContext")
        {
            
        }

        public override int SaveChanges()
        {
            // check added identities and generate Guid
            var changeSet = ChangeTracker.Entries<IEntity>();
            if (changeSet != null)
            {
                foreach (var addedEntry in changeSet.Where(e => e.State == EntityState.Added))
                {
                    if (addedEntry.Entity.Guid == Guid.Empty)
                        addedEntry.Entity.Guid = Guid.NewGuid();
                }
            }

            return base.SaveChanges();
        }

        public DbSet<Employee> Employees { get; set; }
    }

    public class ContextInitializer : DropCreateDatabaseIfModelChanges<EmployeeDbContext>
    {
        protected override void Seed(EmployeeDbContext context)
        {
            context.Employees.Add(new Employee{ Name = "Boss"});
            context.Employees.Add(new Employee{ Name = "Secretary"});
            context.Employees.Add(new Employee{ Name = "Guard"});

            context.SaveChanges();
        }
    }
}
﻿using System;
using System.Collections.ObjectModel;
using SilverlightClient.Model.EmployeeServiceReference;

namespace SilverlightClient.Model.ServiceHelper
{
    /// <summary>
    /// Provides employee service for client.
    /// </summary>
    public class EmployeeServiceHelper : ServiceHelperBase<EmployeeServiceClient>
    {
        /// <summary>
        /// Create a new instance of the <see cref="EmployeeServiceHelper"/> class.
        /// </summary>
        public EmployeeServiceHelper()
        {
            Client = ServiceUtil.EmployeeServiceClient;

            Client.GetAllEmployeesCompleted += (sender, e) => InvokeCallback<ObservableCollection<Employee>>(e);
        }

        /// <summary>
        /// Get all employees.
        /// </summary>
        /// <param name="callback">The callback.</param>
        public void GetAll(Action<ResponseInfo<ObservableCollection<Employee>>> callback)
        {
            Client.GetAllEmployeesAsync(callback);
        }

        /// <summary>
        /// Add new employee.
        /// </summary>
        /// <param name="employee">The new employee.</param>
        public void Add(Employee employee)
        {
            Client.AddAsync(employee);
        }

        /// <summary>
        /// Edit employee.
        /// </summary>
        /// <param name="employee">The new employee.</param>
        public void Edit(Employee employee)
        {
            Client.EditAsync(employee);
        }

        /// <summary>
        /// Delete employee.
        /// </summary>
        /// <param name="employee">The new employee.</param>
        public void Delete(Employee employee)
        {
            Client.RemoveAsync(employee.Id);
        }

        /// <summary>
        /// Save collection of employees.
        /// </summary>
        /// <param name="employees">The employee collection.</param>
        public void Save(ObservableCollection<Employee> employees)
        {
            Client.SaveAsync(employees);
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using SilverlightClient.Model.EmployeeServiceReference;

namespace SilverlightClient.Model.ServiceHelper
{
    /// <summary>
    /// Result of the response.
    /// </summary>
    public class ResponseInfo
    {
        /// <summary>
        /// Create a new instance of the <see cref="ResponseInfo"/> class.
        /// </summary>
        public ResponseInfo() { }

        /// <summary>
        /// Create a new instance of the <see cref="ResponseInfo"/> class.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="exception">The exception.</param>
        public ResponseInfo(string error, Exception exception = null)
        {
            Error = error;
            Exception = exception;
        }

        /// <summary>
        /// Your error string here
        /// </summary>
        public string Error { get; private set; }

        /// <summary>
        /// Your exception here
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Check of the errors (true - none errors)
        /// </summary>
        public static bool CheckError<T>(AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
                return true;

            var action = (Action<ResponseInfo<T>>)e.UserState;

            var faultException = e.Error as FaultException<FaultDetail>;

            if (faultException != null)
            {

                var faultDetailStr = string.Empty;
                var faultDetail = faultException.Detail;
                if (faultDetail != null)
#if DEBUG
                    faultDetailStr = string.Format("Server return error with code {0}, {1}, Stack: {2}", faultDetail.Result, faultDetail.ErrorMessage, faultDetail.ErrorDetails);
#else
                    faultDetailStr = string.Format("Server return error with code {0}, {1}", faultDetail.Result, faultDetail.ErrorMessage);
#endif
                action(new ResponseInfo<T>(string.Format("{0}. {1}", e.Error.Message, faultDetailStr), faultException));
                return false;
            }

            action(new ResponseInfo<T>(e.Error.Message, e.Error));
            return false;
        }

        /// <summary>
        /// Invoke callback with result.
        /// </summary>
        public static void InvokeResult<T>(object userState, T result)
        {
            var action = userState as Action<ResponseInfo<T>>;
            if (action != null)
            {
                action(new ResponseInfo<T>(result));
            }
        }

        /// <summary>
        /// Invoke callback without result.
        /// </summary>
        public static void InvokeResult(object userState)
        {
            var action = userState as Action;
            if (action != null)
            {
                action();
            }
        }
    }

    /// <summary>
    /// Provide response result.
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    public class ResponseInfo<T> : ResponseInfo
    {
        /// <summary>
        /// Create a new instance of the <see cref="ResponseInfo"/> class.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="exception">The exception.</param>
        public ResponseInfo(string error, Exception exception = null) : base(error, exception)
        {
            
        }

        /// <summary>
        /// Create a new instance of the <see cref="ResponseInfo"/> class.
        /// </summary>
        /// <param name="result">The result.</param>
        public ResponseInfo(T result)
        {
            Result = result;
        }

        /// <summary>
        /// Result of action.
        /// </summary>
        public T Result { get; private set; }
    }
}

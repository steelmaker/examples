﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using SilverlightClient.Model.EmployeeServiceReference;

namespace SilverlightClient.Model.ServiceHelper
{
    /// <summary>
    /// Contains methods for handling asyc results
    /// </summary>
    public abstract class ServiceHelperBase<T> where T : class
    {
        protected static T Client;

        /// <summary>
        /// Invoke callback.
        /// </summary>
        /// <typeparam name="TI">The type of the result.</typeparam>
        /// <param name="e">The data for the MethodNameCompleted event.</param>
        protected void InvokeCallback<TI>(AsyncCompletedEventArgs e)
        {
            if (CheckError<TI>(e))
            {
                var type = e.GetType();
                var propertyInfo = type.GetProperty("Result");
                if (propertyInfo == null)
                    throw new Exception("Свойство Result не найдено");
                InvokeResult(e.UserState, (TI)propertyInfo.GetValue(e, null));
            }
        }

        /// <summary>
        /// Check of the errors (true - none errors)
        /// </summary>
        private static bool CheckError<TI>(AsyncCompletedEventArgs e)
        {
            if (e.Error == null) return true;

            var action = (Action<ResponseInfo<TI>>)e.UserState;
            var exception = e.Error as FaultException<FaultDetail>;
            if (exception != null)
                action(new ResponseInfo<TI>(exception.Detail.ErrorMessage));
            else
                action(new ResponseInfo<TI>(e.Error.Message));
            return false;
        }

        /// <summary>
        /// Invoke callback with result.
        /// </summary>
        private static void InvokeResult<TI>(object userState, TI result)
        {
            var action = (Action<ResponseInfo<TI>>)userState;
            action(new ResponseInfo<TI>(result));
        }
    }
}

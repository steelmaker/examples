﻿using System;
using System.ServiceModel;
using System.Windows;
using SilverlightClient.Model.EmployeeServiceReference;

namespace SilverlightClient.Model.ServiceHelper
{
    /// <summary>
    /// Provide clients of services.
    /// </summary>
    public static class ServiceUtil
    {
        private static readonly TimeSpan MTimeout = new TimeSpan(0, 30, 0);
        private const int CBufferSize = int.MaxValue;

        /// <summary>
        /// Gets is secure.
        /// </summary>
        public static bool IsSecure
        {
            get { return Application.Current.Host.Source.Scheme == "https"; }
        }

        /// <summary>
        /// Gets endpoint address.
        /// </summary>
        /// <param name="relativeServiceUrl">The url.</param>
        /// <returns></returns>
        internal static EndpointAddress GetAddress(string relativeServiceUrl)
        {
            return new EndpointAddress(new Uri(new Uri("http://localhost:58204/"), relativeServiceUrl));
            //return new EndpointAddress(new Uri(Application.Current.Host.Source, relativeServiceUrl));
        }

        /// <summary>
        /// Gets the http binding.
        /// </summary>
        internal static BasicHttpBinding CurrentBinding
        {
            get
            {
                var binding = new BasicHttpBinding
                {
                    MaxBufferSize = CBufferSize,
                    MaxReceivedMessageSize = CBufferSize,
                    ReceiveTimeout = MTimeout,
                    OpenTimeout = MTimeout,
                    SendTimeout = MTimeout,
                    CloseTimeout = MTimeout,
                    TransferMode = TransferMode.Buffered
                };

                binding.Security.Mode = IsSecure ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.TransportCredentialOnly;

                return binding;
            }
        }

        /// <summary>
        /// Client of the employee service.
        /// </summary>
        public static EmployeeServiceClient EmployeeServiceClient
        {
            get { return new EmployeeServiceClient(CurrentBinding, GetAddress("../EmployeeService.svc")); }
        }
    }
}

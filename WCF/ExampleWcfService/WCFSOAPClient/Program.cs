﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WCFSOAPClient.EmployeeServiceReference;

namespace WCFSOAPClient
{
    class Program
    {
        static void Main()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            Task<List<Employee>> task = new Task<List<Employee>>(GetData, cancellationTokenSource.Token);

            Console.WriteLine("Loading...");

            task.Start();


            var employees = task.Result;

            Console.WriteLine("Loading complete.");
            Thread.Sleep(500);
            Console.WriteLine("Result:");

            foreach (var employee in employees)
            {
                Console.WriteLine(employee.Name);
            }

            var newName = Console.ReadLine();

            employees[0].Name = newName;
            var editTask = new Task(() => EditData(employees[0]));
            editTask.Start();

            Console.ReadKey();
        }

        private static void EditData(Employee employee)
        {
            new EmployeeServiceClient().Edit(employee);
        }

        private static List<Employee> GetData()
        {
            return new EmployeeServiceClient().GetAllEmployeess().ToList();
        }
    }
}

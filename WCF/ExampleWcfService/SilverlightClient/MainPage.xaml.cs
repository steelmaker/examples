﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using SilverlightClient.Model.EmployeeServiceReference;
using SilverlightClient.Model.ServiceHelper;

namespace SilverlightClient
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            Loaded += (sender, args) => Refresh();
        }

        private void RefreshOnClick(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void AddOnClick(object sender, RoutedEventArgs e)
        {
            AddItem(new Employee {Name = "New employee"});
        }

        private void DeleteOnClick(object sender, RoutedEventArgs e)
        {
            var employees = ItemsControl.ItemsSource as ObservableCollection<Employee>;
            if (employees == null)
                return;

            var selectedEmploye = ItemsControl.SelectedItem as Employee;
            if (selectedEmploye == null)
                return;

            employees.Remove(selectedEmploye);

            ModifiedItem(new EmployeeServiceHelper().Delete, selectedEmploye);
        }

        private void SaveOnClick(object sender, RoutedEventArgs e)
        {
            var employees = ItemsControl.ItemsSource as ObservableCollection<Employee>;
            if (employees == null)
                return;

            new EmployeeServiceHelper().Save(employees);
        }

        private void Refresh()
        {
            new EmployeeServiceHelper().GetAll(info =>
            {
                ItemsControl.ItemsSource = info.Result;
            });
        }

        private void AddItem(Employee item)
        {
            if (item == null)
                return;

            var items = ItemsControl.ItemsSource as ObservableCollection<Employee>;
            if (items == null)
                return;

            items.Add(item);
        }

        private void ModifiedItem(Action<Employee> action, Employee item = null)
        {
            var employee = item ?? ItemsControl.SelectedItem as Employee;
            if (employee == null)
                return;

            action(employee);
        }
    }
}
